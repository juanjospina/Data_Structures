/*
    bt2iter_threaded.h
    <erase instructions and finish file header info here>

    Instructions:

    1. Complete the implementation of Iterator support methods.  Do not
       copy/paste ANY code - type everyting, one character at a time, working
       from memory and understanding. Consult lecture notes only when stuck, but
       STILL type it, do not copy it.
       
    2. Study all the other implementations especially those of the public
       methods. 
*/

/*
    <file documentation - requires modification>

    binary tree iterator classes designed to operate on 2-way trees
    (no parent pointer)

    Container class C is assumed to define the following Node methods:

      IsDead()
      HasLeftChild()
      HasRightChild()
      IsLeftThreaded()
      IsRightThreaded()

    Iterator support required in C:

    Iterator Begin() const
    {
      Iterator i;
      i.Init(root_);
      return i;
    }

    Iterator End() const
    {
      Iterator i;
      return i;
    }

    Iterator rBegin() const
    {
      Iterator i;
      i.rInit(root_);
      return i;
    }

    Iterator rEnd() const
    {
      Iterator i;
      return i;
    }

*/

// #include <cstddef>     // size_t
// #include <cstdint>     // uint8_t
// #include <ansicodes.h>
// #include <iostream>
// #include <iomanip>
// #include <compare.h>   // LessThan

#include <queue.h>     // LevelorderIterator,  Dump()
#include <stack.h>     // InorderIterator deque-based stack
#include <vector.h>    // InorderIterator alternative
#include <list.h>      // InorderIterator alternative
#include <debug.h>     // argh

#ifndef _BT2ITER_THREADED_H
#define _BT2ITER_THREADED_H

namespace fsu
{

  template < class C >
  class ThreadedBTIterator;   // patterns: ConstIterator, BidirectionalIterator

  /*****************************************************/
  /*        class ThreadedBTIterator < C >             */
  /*****************************************************/

  template < class C >
  class ThreadedBTIterator // a ConstIterator pattern
  {
  private:
    friend   C;
    typename C::Node* node_;

  public:
    // terminology support
    typedef typename C::ValueType    ValueType;
    typedef typename C::Node         Node;
    typedef ThreadedBTIterator<C>    ConstIterator;
    typedef ThreadedBTIterator<C>    Iterator;

    // constructors
             ThreadedBTIterator  ();
    virtual  ~ThreadedBTIterator ();
             ThreadedBTIterator  (Node* n);    // type converter
             ThreadedBTIterator  (const ThreadedBTIterator& i); // copy ctor

    // information/access
    bool           Valid   () const; // cursor is valid element

    // various operators
    bool                    operator == (const ThreadedBTIterator& i2) const;
    bool                    operator != (const ThreadedBTIterator& i2) const;
    const ValueType&        operator *  () const; // const version
    ThreadedBTIterator<C>&  operator =  (const ThreadedBTIterator& i);
    ThreadedBTIterator<C>&  operator ++ ();    // prefix
    ThreadedBTIterator<C>   operator ++ (int); // postfix
    ThreadedBTIterator<C>&  operator -- ();    // prefix
    ThreadedBTIterator<C>   operator -- (int); // postfix

  private:
    void Init        (Node*);   
    void rInit       (Node*);
    void Increment   ();  // moves to next inorder node
    void Decrement   ();  // moves to previous inorder node

  };

  // protected increment/decrement
  template < class C >
  void ThreadedBTIterator<C>::Init(Node* n)
  {
    node_ = n;
    // *****************
    // missing code here
    // *****************
  }

  template < class C >
  void ThreadedBTIterator<C>::rInit(Node* n)
  {
    node_ = n;
    // *****************
    // missing code here
    // *****************
  }

  template < class C >
  void ThreadedBTIterator<C>::Increment ()
  {
    if (node_ == nullptr) return;
    // *****************
    // missing code here
    // *****************
  }

  template < class C >
  void ThreadedBTIterator<C>::Decrement ()
  {
    if (node_ == nullptr) return;
    // *****************
    // missing code here
    // *****************
  }

  template < class C >
  ThreadedBTIterator<C>::ThreadedBTIterator ()  :  node_()
  {}

  template < class C >
  ThreadedBTIterator<C>::~ThreadedBTIterator () 
  {}

  template < class C >
  ThreadedBTIterator<C>::ThreadedBTIterator (const ThreadedBTIterator<C>& i)
    :  node_(i.node_)
  {}

  template < class C >
  ThreadedBTIterator<C> & ThreadedBTIterator<C>::operator =  (const ThreadedBTIterator<C>& i)
  {
    node_ = i.node_;
    return *this;
  }

  template < class C >
  ThreadedBTIterator<C>::ThreadedBTIterator (Node * n)
    :  node_(n)
  {}

  template < class C >
  bool ThreadedBTIterator<C>::Valid() const
  {
    return node_ != 0;
  }

  template < class C >
  ThreadedBTIterator<C> & ThreadedBTIterator<C>::operator ++()
  {
    do Increment();
    while (node_ != nullptr && node_->IsDead());
    return *this;
  }

  template < class C >
  ThreadedBTIterator<C>   ThreadedBTIterator<C>::operator ++(int)
  {
    ThreadedBTIterator<C> i = *this;
    operator ++();
    return i;
  }

  template < class C >
  ThreadedBTIterator<C> & ThreadedBTIterator<C>::operator --()
  {
    do Decrement();
    while (node_ != nullptr && node_->IsDead());
    return *this;
  }

  template < class C >
  ThreadedBTIterator<C>   ThreadedBTIterator<C>::operator --(int)
  {
    ThreadedBTIterator<C> i = *this;
    operator --();
    return i;
  }

  template < class C >
  const typename C::ValueType& ThreadedBTIterator<C>::operator *() const
  {
    return node_->value_;
  }

  template < class C >
  bool ThreadedBTIterator<C>::operator == (const ThreadedBTIterator<C>& i2) const
  {
    return node_ == i2.node_;
  }

  template < class C >
  bool ThreadedBTIterator<C>::operator != (const ThreadedBTIterator<C>& i2) const
  {
    return !(*this == i2);
  }

} // namespace fsu 

#endif
