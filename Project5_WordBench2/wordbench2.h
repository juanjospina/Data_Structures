/**
*     @file    wordbench2.h
*     @author  Juan Jose Ospina
*     @date    2015-06-13
*     @version 1.0
*/                 

#include <iomanip>
#include <fstream>
#include <xstring.h>
#include <list.h>
#include <oaa.h>
//#include <wordify.cpp>
#include <compare.h> 


class WordBench
{


   public:

      WordBench           ();
      virtual ~WordBench  ();
      bool   ReadText     (const fsu::String& infile);
      bool   WriteReport  (const fsu::String& outfile, unsigned short kw = 15, unsigned short dw = 15, std::ios_base::fmtflags kf = std::ios_base::left, std::ios_base::fmtflags df = std::ios_base::right)  const;
      void   ShowSummary  () const;
      void   ClearData    ();


   private:

      typedef 		fsu::String KeyType;
      typedef size_t 	DataType;
      
      size_t            count_;
      fsu::OAA  < KeyType , DataType > frequency_;
      fsu::List < fsu::String >        infiles_;
      
      //static void Wordify  (fsu::String& s); // optional

};
