/*
*     @file    wordbench2.cpp
*     @author  Juan Jose Ospina
*     @date    2015-06-21
*     @version 2.0
*/                 

#include <wordify.cpp>
#include <wordbench2.h>

// Constructor
WordBench::WordBench()
{

}

// Destructor
WordBench::~WordBench()
{
   ClearData();
}

/*
*    Function : ReadText()
*    Description: Reads the strings of a file
*    and enters them into a structure OAA. wordbench
*    Author: Juan Jose Ospina
*    Date: June 21, 2015
*/


bool WordBench::ReadText(const fsu::String& infile)
{

   std::ifstream in1;
   fsu::String tempStr;
   int insertCount=0;

   in1.open(infile.Cstr());
   if(!in1)
      return 0;

   infiles_.PushBack(infile);

   while(in1 >> tempStr)
   {

      if(fsu::String::StrCmp(tempStr, "") != 0)
      {
         Wordify(tempStr);     
	  
         if(tempStr.Length() != 0 )
         {
	    ++frequency_[tempStr];
	    ++count_;
	    ++insertCount;
         }
      }
    }

   //	Report Summary
   std::cout << "\n\n     Number of words read:    " << insertCount 
   << "\n     Current vocabulary size: " << frequency_.Size()
   << "\n\n";

   if(frequency_.Empty()== true)
      return 0;
   else 
      return 1;
}

/*
*    Function : WriteReport()
*    Description: Writes the report of the wordbench
*    into a file provided by the user.
*    Author: Juan Jose Ospina
*    Date: June 21, 2015
*/

bool  WordBench::WriteReport (const fsu::String& outfile, unsigned short kw, unsigned short dw, std::ios_base::fmtflags kf, std::ios_base::fmtflags df) const
{


   // Output variables
   const char ofr = ' ';

   std::ofstream out1(outfile.Cstr());
   if (out1.fail())
     {
        std::cout << " ** cannot open file " << outfile << " for write\n"
	<< " ** try again\n";
	return 0;
     }

   out1 << "Text Analysis for files: " ;
   infiles_.Display(out1, ofr);

   out1 << "\n\nword" << std::setw(30)  << "frequency\n"
       << "----" << std::setw(30) << "--------\n";

   frequency_.Display(out1, kw, dw, kf, df); 


   out1 <<"\n\nNumber of words: " << count_ 
   << "\nVocabulary size: " << frequency_.Size() << std::endl;

   std::cout <<"\n     Number of words: " << count_
      << "\n     Vocabulary size: " << frequency_.Size() <<std::endl
      << "     Analyis written to file " << outfile << "\n\n"; 


   return 1;

}


/*
*    Function : ShowSummary()
*    Description: Shows summary of wordbench
*    into standard output - screen.
*    Author: Juan Jose Ospina
*    Date: June 21, 2015
*/

void WordBench::ShowSummary () const
{


   // Output variables
   std::ostream* osr = &std::cout;
   const char ofr = ',';
   
   std::cout << "\n     Current Files: ";
   infiles_.Display(*osr, ofr);
   std::cout << "\n     Current word count:    " << count_
   << "\n     Current vocabulary size: " << frequency_.Size() << "\n";



}

/*
*    Function : ClearData()
*    Description: Clear all data
*    Author: Juan Jose Ospina
*    Date: June 21, 2015
*/

void WordBench::ClearData()
{

   // Clear List
   infiles_.Clear();
   //Clear Set
   frequency_.Clear();
   //Clear Words Counted
   count_ = 0;

}




