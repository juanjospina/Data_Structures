/**
   @file    post.h
   @author  Juan Jose Ospina
   @date    2015-05-29
   @version 1.0

   Implementation of the Post Machine.
   An abstract computer which receives
   instructions in the next manner:

   <current_state><ch><next_state><word>.
                             
*/

#ifndef _POST_H
#define _POST_H

#include <iostream>
#include <fstream>
#include <iomanip>
#include <list.h>
#include <queue.h>
#include <xstring.h>

namespace post
{

	const size_t maxIterations = 10000;

}


class PostMachine
{
	public:

		bool Load    (bool batch = 0);
		void Run     (bool batch = 0);
		PostMachine();
		~PostMachine();
		
	private:

		fsu::List  < fsu::String >                      program_;
		fsu::Queue < char , fsu::Deque < char> >        tape_;
		char                                            state_;

};

#endif

