/**
  @file    post.cpp
  @author  Juan Jose Ospina
  @date    2015-05-29
  @version 1.0

  Implementation of the Post Machine.
  An abstract computer which receives
  instructions in the next manner:

  <current_state><ch><next_state><word>.

  The programs it requires consist on a
  list of instructions.

  After a processing sequence, a string is
  either accepted, rejected, or the system
  crash.
  
 */

#include <post.h>

/*
    Function : ClearBuffer()
    Description: Clear an istream. 
    		 Provided by Instructor.
    Author: Chris Lacher
*/

void ClearBuffer(std::istream& is)
{
	char ch;

	do
	{
		is.get(ch);
	}
	while ((!is.eof()) && (ch != '\n'));
}

/*
   Function : PostMachine()
   Description: Announcing Constructor 
   Author: Juan Jose Ospina
   Date: May 29, 2015
*/

PostMachine::PostMachine()
{
	std::cout << "\n** Post Machine started.\n\n";

}

/*
   Function : ~PostMachine()
   Description: Announcing Destructor 
   Author: Juan Jose Ospina
   Date: May 29, 2015
*/

PostMachine::~PostMachine()
{
	std::cout << "\n** Post Machine stopped.\n\n";

}


/*
   Function : Load()
   Description: Loads a program instructions
   		from to the program_ List.
   Author: Juan Jose Ospina
   Date: May 29, 2015
*/

bool PostMachine::Load(bool batch)
{

	// I/O variables
	std::ifstream in1;
	fsu::String filename;
	fsu::String tempStr;	//temporal String

	// Clearing the Lists
	program_.Clear();
	tape_.Clear();

	do
	{
		in1.clear();

		// open program file
		std::cout << "  Name of instruction file: ";
		std::cin >> filename;
		if (batch) std::cout << filename << '\n';

		in1.open(filename.Cstr());

		if(!in1)
			std::cout << "Invalid File, Try Again,\n";

	}while(!in1);

	do
	{
		tempStr.GetLine(in1);
		program_.PushBack(tempStr);

	}while ((!in1.eof())); 


	if(program_.Empty() == 1)
		return 0;
	else
		return 1;

}


/*
   Function : Run()
   Description: Runs the processing algorithm.
   		The execution is made up of cycles
		which search for matches between
		the program_ and the current state-
		next state of the machine.
   Author: Juan Jose Ospina
   Date: May 29, 2015
*/


void PostMachine::Run(bool batch)
{
	char ch;

	// simulation variables
	bool finished, halt, crash, maxits_reached, match;  // booleans
	size_t its = 0;
	
	// Output variables
	std::ostream* osr = &std::cout; 
	const char ofr = '\0';


	// now loop until '*' is received as (first character of) input
	while(1)
	{
		if(its == 0)
			std::cin.ignore(1);

		// empty the tape
		tape_.Clear();

		// read input and place on tape, followed by '#'
		// if first char is '*', purge input buffer and return - Run is over

		std::cout << "   Input string (* to end): ";
		ch = std::cin.get();
		if (batch) std::cout.put(ch);

		if (ch == '*')
		{
			ClearBuffer(std::cin);
			if (batch) std::cout.put('\n');
			return;
		}


		// User Input
		while (ch != '\n' )
		{	
			tape_.Push(ch);
			ch = std::cin.get();
			if (batch) std::cout.put(ch);
		} 
		if (batch) std::cout.put('\n');
		tape_.Push('#');

		// run Post machine
		state_ = 'S';		// Internal State
		finished = halt = crash = maxits_reached = 0;
		its = 0;

		// Cycle Through Program
		int chPos = 0;		// 0th Position/Instruction.
		int chPos1 = 1;		// 1st Position/Instruction.
		int chPos2 = 2;		// 2th Position/Instruction.
		int LengthPos = 0;      // Length of String.

		do
		{

			match = 0;
			for (typename fsu::List<fsu::String>::ConstIterator i = program_.Begin(); i != program_.End(); ++i) 
			{

				LengthPos = (*i).Length();

				if(((*i).Element(chPos) != '*') && ((*i).Element(chPos) != '\0'))
				{	
					if(((*i).Element(chPos) == state_) && ((*i).Element(chPos1) == tape_.Front()))
					{
						tape_.Pop();			
						state_ = (*i).Element(chPos2);
						for(int j = chPos2 +1 ; j < LengthPos ; ++j)
							tape_.Push((*i).Element(j));
						match = 1;
					}
				}
			}

			++its;  
			halt           = (state_ == 'H');
			crash          = !match;
			maxits_reached = (its == post::maxIterations);
			finished       = halt || crash || maxits_reached;
		}
		while (!finished);

		// report results
		if (halt)
		{
			std::cout << std::setw(25) << std::right
			<< "String accepted.\n" << std::setw(25) 
			<< std::right << "Tape contents at halt " 
			<< std::left << " : ";
			tape_.Display(*osr, ofr);
			std::cout << std::endl << std::endl;
		}
		else if (crash)
		{
			std::cout << std::setw(25) << std::right
			<<"String rejected.\n" << std::setw(25) 
			<< std::right << "Last State " << std::left 
			<<" : " << state_ << "\n" << std::setw(25) 
			<< std::right << "Tape contents at rejection "
			<< std::left << " : ";
			tape_.Display(*osr, ofr);
			std::cout << std::endl << std::endl;
		}
		else if (maxits_reached)
		{
			std::cout << "	Machine stopped after " << its 
			<< " iterations \n" 
			<< "	Tape contents when stopped: ";
			tape_.Display(*osr, ofr);
			std::cout << "\n	Last State: " << state_ 
			<< "\n\n";

		}
		else // presumed unreachable branch 
		{
			std::cerr << "** PostMachine error: bad processing termination\n";
		}

	} // end while(1)

} // end Run()




