/*
   @file    bt2iter_threaded.h
   @author  Juan Jose Ospina
   @date    2015-07-07
   @version 1.5

   This file contains the structural implementations of the iterators
   used on threaded binary search trees (bst). It has all the important
   definitions that an API of an iterator would have like :

   1. Initialization : Init
   2. Increment - paired with operator++ (pre and post)
   3. Decrement - paired with operator-- (pre and post)
   4. Reverse Initialization - rInit

*/


// #include <cstddef>     // size_t
// #include <cstdint>     // uint8_t
// #include <ansicodes.h>
// #include <iostream>
// #include <iomanip>
// #include <compare.h>   // LessThan

#include <queue.h>     // LevelorderIterator,  Dump()
#include <stack.h>     // InorderIterator deque-based stack
#include <vector.h>    // InorderIterator alternative
#include <list.h>      // InorderIterator alternative
#include <debug.h>     // argh

#ifndef _BT2ITER_THREADED_H
#define _BT2ITER_THREADED_H

namespace fsu
{

  template < class C >
  class ThreadedBTIterator;   // patterns: ConstIterator, BidirectionalIterator

  /*****************************************************/
  /*        class ThreadedBTIterator < C >             */
  /*****************************************************/

  template < class C >
  class ThreadedBTIterator // a ConstIterator pattern
  {
  private:
    friend   C;
    typename C::Node* node_;

  public:
    // terminology support
    typedef typename C::ValueType    ValueType;
    typedef typename C::Node         Node;
    typedef ThreadedBTIterator<C>    ConstIterator;
    typedef ThreadedBTIterator<C>    Iterator;

    // constructors
             ThreadedBTIterator  ();
    virtual  ~ThreadedBTIterator ();
             ThreadedBTIterator  (Node* n);    // type converter
             ThreadedBTIterator  (const ThreadedBTIterator& i); // copy ctor

    // information/access
    bool           Valid   () const; // cursor is valid element

    // various operators
    bool                    operator == (const ThreadedBTIterator& i2) const;
    bool                    operator != (const ThreadedBTIterator& i2) const;
    const ValueType&        operator *  () const; // const version
    ThreadedBTIterator<C>&  operator =  (const ThreadedBTIterator& i);
    ThreadedBTIterator<C>&  operator ++ ();    // prefix
    ThreadedBTIterator<C>   operator ++ (int); // postfix
    ThreadedBTIterator<C>&  operator -- ();    // prefix
    ThreadedBTIterator<C>   operator -- (int); // postfix

  private:
    void Init        (Node*);   
    void rInit       (Node*);
    void Increment   ();  // moves to next inorder node
    void Decrement   ();  // moves to previous inorder node

  };


/*
 * Function : Init()
 * Description: Initialization of Threaded Iterator
 * Author: Juan Jose Ospina
 * Date: July 7, 2015
 */

  template < class C >
  void ThreadedBTIterator<C>::Init(Node* n)
  {
    node_ = n;
    
    while(node_ != nullptr && node_->HasLeftChild() && !node_->IsLeftThreaded())
       node_ = node_->lchild_;

    while(Valid() && node_->IsDead())
       Increment();
  }

/*
 * Function : rInit()
 * Description: Reverse Initialization of Threaded Iterator
 * Author: Juan Jose Ospina
 * Date: July 7, 2015
 */

  template < class C >
  void ThreadedBTIterator<C>::rInit(Node* n)
  {
    node_ = n;
    while(node_ != nullptr && node_->HasRightChild() && !node_->IsRightThreaded())
       node_ = node_->rchild_;

    while(Valid() && node_->IsDead())
       Decrement();
  }

/*
 * Function : Increment()
 * Description: Increments iterator using Threads 
 * Author: Juan Jose Ospina
 * Date: July 7, 2015
 */

  template < class C >
  void ThreadedBTIterator<C>::Increment ()
  {
    if (node_ == nullptr) return;
    if (node_->IsRightThreaded())
    {
       node_ = node_->rchild_;
       return;
    }
   
    node_ = node_->rchild_;
    while(node_ != nullptr && node_->HasLeftChild() && !node_->IsLeftThreaded())
       node_ = node_->lchild_;
       
  }

/*
 * Function : Decrement()
 * Description: Decrement iterator using Threads 
 * Author: Juan Jose Ospina
 * Date: July 7, 2015
 */

  template < class C >
  void ThreadedBTIterator<C>::Decrement ()
  {
    if (node_ == nullptr) return;
    if (node_->IsLeftThreaded())
    {
       node_ = node_->lchild_;
       return;
    }
    node_ = node_->lchild_;
    while(node_ != nullptr && node_->HasRightChild() && !node_->IsRightThreaded())
       node_ = node_->rchild_;
  }

  template < class C >
  ThreadedBTIterator<C>::ThreadedBTIterator ()  :  node_()
  {}

  template < class C >
  ThreadedBTIterator<C>::~ThreadedBTIterator () 
  {}

  template < class C >
  ThreadedBTIterator<C>::ThreadedBTIterator (const ThreadedBTIterator<C>& i)
    :  node_(i.node_)
  {}

  template < class C >
  ThreadedBTIterator<C> & ThreadedBTIterator<C>::operator =  (const ThreadedBTIterator<C>& i)
  {
    node_ = i.node_;
    return *this;
  }

  template < class C >
  ThreadedBTIterator<C>::ThreadedBTIterator (Node * n)
    :  node_(n)
  {}

  template < class C >
  bool ThreadedBTIterator<C>::Valid() const
  {
    return node_ != 0;
  }

  template < class C >
  ThreadedBTIterator<C> & ThreadedBTIterator<C>::operator ++()
  {
    do Increment();
    while (node_ != nullptr && node_->IsDead());
    return *this;
  }

  template < class C >
  ThreadedBTIterator<C>   ThreadedBTIterator<C>::operator ++(int)
  {
    ThreadedBTIterator<C> i = *this;
    operator ++();
    return i;
  }

  template < class C >
  ThreadedBTIterator<C> & ThreadedBTIterator<C>::operator --()
  {
    do Decrement();
    while (node_ != nullptr && node_->IsDead());
    return *this;
  }

  template < class C >
  ThreadedBTIterator<C>   ThreadedBTIterator<C>::operator --(int)
  {
    ThreadedBTIterator<C> i = *this;
    operator --();
    return i;
  }

  template < class C >
  const typename C::ValueType& ThreadedBTIterator<C>::operator *() const
  {
    return node_->value_;
  }

  template < class C >
  bool ThreadedBTIterator<C>::operator == (const ThreadedBTIterator<C>& i2) const
  {
    return node_ == i2.node_;
  }

  template < class C >
  bool ThreadedBTIterator<C>::operator != (const ThreadedBTIterator<C>& i2) const
  {
    return !(*this == i2);
  }

} // namespace fsu 

#endif
