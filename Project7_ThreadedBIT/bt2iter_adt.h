/*
    @file    bt2iter_adt.h
    @author  Juan Jose Ospina
    @date    2015-07-05
    @version 1.0

   This file contains the structural definition of the base used for the
   iterators present on a binary search tree. The four essential traversals
   that this iterators are focused on are:

   1. Level Order iterator (Uses stack as primary order)
   2. Inorder Iterator (Uses stack to order in the correct manner)
   3. Preorder Iterator
   4. Postorder Iterator


*/

// #include <cstddef>     // size_t
// #include <cstdint>     // uint8_t
// #include <ansicodes.h>
// #include <iostream>
// #include <iomanip>
// #include <compare.h>   // LessThan

#include <queue.h>     // LevelorderIterator,  Dump()
#include <stack.h>     // InorderIterator deque-based stack
#include <vector.h>    // InorderIterator alternative
#include <list.h>      // InorderIterator alternative
#include <debug.h>     // argh

#ifndef _BT2ITER_ADT_H
#define _BT2ITER_ADT_H

namespace fsu
{

  template < class C >
  class InorderBTIterator;     // patterns: ConstIterator, BidirectionalIterator

  template < class C >
  class PreorderBTIterator;    // patterns: ConstIterator, BidirectionalIterator

  template < class C >
  class PostorderBTIterator;   // patterns: ConstIterator, BidirectionalIterator

  template < class C >
  class LevelorderBTIterator;  // patterns: ConstIterator, ForwardIterator

  /******************************************************************/
  /*    LevelorderBTIterator < C >                                  */
  /******************************************************************/

  template < class C >
  class LevelorderBTIterator // patterns: ConstIterator, BidirectionalIterator
  {
  public: // terminology support
    typedef typename C::ValueType    ValueType;
    typedef typename C::Node         Node;
    typedef LevelorderBTIterator<C>  ConstIterator;
    typedef LevelorderBTIterator<C>  Iterator;

  private: // inner sanctum
    friend C;
    fsu::Queue < Node* > que_; // default is deque-based

  public:
    // first class
    LevelorderBTIterator                 () : que_() {}
    virtual  ~LevelorderBTIterator       () { que_.Clear(); }
    LevelorderBTIterator                 (const LevelorderBTIterator& i) : que_(i.que_) {}
    LevelorderBTIterator<C>&  operator=  (const LevelorderBTIterator& i) { que_ = i.que_; return *this; }

    // information/access
    bool  Valid   () const { return !que_.Empty(); } // Iterator can be de-referenced

    // various operators
    bool                      operator== (const LevelorderBTIterator& i2) const { return que_ == i2.que_; }
    bool                      operator!= (const LevelorderBTIterator& i2) const { return !(*this == i2); }
    const ValueType&          operator*  () const { return que_.Front()->value_; }
    LevelorderBTIterator<C>&  operator++ ();    // prefix
    LevelorderBTIterator<C>   operator++ (int); // postfix

  private:
    void Init      (Node* n);
    void Increment ();
  };

  template < class C >
  void LevelorderBTIterator<C>::Init(Node* n)
  {
    que_.Clear();
    if (n == nullptr) return;
    que_.Push(n);
    while (!que_.Empty() && que_.Front()->IsDead())
      Increment();
  }


/*
 *    Function : Increment()
 *    Description: Increments LevelOrder iterator
 *    Author: Juan Jose Ospina
 *    Date: July 5, 2015
 */

  template < class C >
  void LevelorderBTIterator<C>::Increment()
   {
    // fsu::Debug("Increment()");
    if ( que_.Empty() )
      return;
    else
    {
       if(que_.Front()->HasLeftChild())
          que_.Push((que_.Front())->lchild_);
       if(que_.Front()->HasRightChild())
          que_.Push((que_.Front())->rchild_);
       
       que_.Pop();
       		
    }
  }

  template < class C >
  LevelorderBTIterator<C>&  LevelorderBTIterator<C>::operator++ ()
  {
    // fsu::Debug("operator++()");
    do Increment();
    while (!que_.Empty() && que_.Front()->IsDead());
    return *this;
  }

  template < class C >
  LevelorderBTIterator<C>   LevelorderBTIterator<C>::operator++ (int)
  {
    LevelorderBTIterator<C> i(*this);
    this->operator++();
    return i;
  }

  /******************************************************************/
  /*    InorderBTIterator < C >                                     */
  /******************************************************************/

  template < class C >
  class InorderBTIterator // patterns: ConstIterator, BidirectionalIterator
  {
  public: // terminology support
    typedef typename C::ValueType    ValueType;
    typedef typename C::Node         Node;
    typedef InorderBTIterator<C>     ConstIterator;
    typedef InorderBTIterator<C>     Iterator;

  private: // inner sanctum
    friend C;
    //fsu::Stack < Node* > stk_; // default is deque-based - better safety & error detection
     fsu::Stack < Node* , fsu::Vector < Node* > > stk_; // faster

  public:
    // first class
    InorderBTIterator                 () : stk_() {}
    virtual  ~InorderBTIterator       () { stk_.Clear(); }
    InorderBTIterator                 (const InorderBTIterator& i) : stk_(i.stk_) {}
    InorderBTIterator<C>&  operator=  (const InorderBTIterator& i) { stk_ = i.stk_; return *this; }

    // information/access
    bool  Valid   () const { return !stk_.Empty(); } // Iterator can be de-references

    // various operators
    bool                   operator== (const InorderBTIterator& i2) const { return stk_ == i2.stk_; }
    bool                   operator!= (const InorderBTIterator& i2) const { return !(*this == i2); }
    const ValueType&       operator*  () const { return stk_.Top()->value_; }
    InorderBTIterator<C>&  operator++ ();    // prefix
    InorderBTIterator<C>   operator++ (int); // postfix
    InorderBTIterator<C>&  operator-- ();    // prefix
    InorderBTIterator<C>   operator-- (int); // postfix

  private:
    void Init      (Node* n); // live nodes only
    void sInit     (Node* n); // structural (all nodes) version - pair with Increment for structural traversal
    void rInit     (Node* n); // live nodes only
    void Increment (); // structural version of ++
    void Decrement (); // structural version of --
  };


/*
 *    Function : sInit()
 *    Description: Structural Initialization
 *    Author: Juan Jose Ospina
 *    Date: July 5, 2015
 */

  template < class C >
  void InorderBTIterator<C>::sInit(Node* n)
  {
    // Structural initialization. This is used to initialize at first
    // whether node is alive or dead.
    if (n == nullptr) return;
    stk_.Clear();
    stk_.Push(n);

    while(n != nullptr && n->HasLeftChild())
    {
       n = n->lchild_;
       stk_.Push(n);
    }

  }

/*
 *    Function : Init()
 *    Description: Initialization
 *    Author: Juan Jose Ospina
 *    Date: July 5, 2015
 */

  template < class C >
  void InorderBTIterator<C>::Init(Node* n)
  {
    /*This is used to initialize a firt live node */
    if (n == nullptr) return;
    stk_.Clear();
    stk_.Push(n);
    
    while(n != nullptr && n->HasLeftChild())
    {
       n = n->lchild_;
       stk_.Push(n);
    }

    while(Valid() && stk_.Top()->IsDead())
       Increment();

  }


/*
 *    Function : rInit()
 *    Description: Reverse Initialization
 *    Author: Juan Jose Ospina
 *    Date: July 5, 2015
 */

  template < class C >
  void InorderBTIterator<C>::rInit(Node* n)
  {
    /* reverse initialization used to start at the "last" live node */

    stk_.Clear();
    if (n == nullptr) return;
    stk_.Push(n);

    while(n != nullptr && n->HasRightChild())
    {
       n = n->rchild_;
       stk_.Push(n);
    }

    while(Valid() && stk_.Top()->IsDead())
       Decrement();

  }

/*
 *    Function : Increment()
 *    Description: Increments the iterator
 *    Author: Juan Jose Ospina
 *    Date: July 5, 2015
 */

  template < class C >
  void InorderBTIterator<C>::Increment()
  {

    // fsu::Debug("Increment()");
    if ( stk_.Empty() )
      return;
   
    Node* loc; // saving the root of the iteration
    Node* parent;
    bool wasRight;

    if(stk_.Top()->HasRightChild())
    {
       loc = stk_.Top()->rchild_; // go to right child
       stk_.Push(loc);
      while(loc != nullptr && loc->HasLeftChild()) //left slide 
       {
          loc = loc->lchild_;
          stk_.Push(loc);
       }
     }
     else 
     {// ASCEND BRANCH
        do
	{ 
	   loc = stk_.Top();
	   stk_.Pop();
	   if(stk_.Empty())
	      parent = nullptr;
	   else
	      parent = stk_.Top();
	   wasRight = (parent != nullptr && parent->HasRightChild() && loc == parent->rchild_);
        }while(wasRight); // location was the right child of parent
    }
    
  }


/*
 *    Function : Decrement()
 *    Description: Decrements the iterator
 *    Author: Juan Jose Ospina
 *    Date: July 5, 2015
 */

  template < class C >
  void InorderBTIterator<C>::Decrement()
  {
    // fsu::Debug("Decrement()");
    if ( stk_.Empty() )
      return;
    
    Node* loc; // saving the root of the iteration
    Node* parent;
    bool wasLeft;

    if(stk_.Top()->HasLeftChild())
    {
       loc = stk_.Top()->lchild_; // go to left child
       stk_.Push(loc);
      while(loc != nullptr && loc->HasRightChild()) // right slide 
       {
          loc = loc->rchild_;
          stk_.Push(loc);
       }
     }
     else 
     {
        do
	{ 
	   loc = stk_.Top();
	   stk_.Pop();
	   if(stk_.Empty())
	      parent = nullptr;
	   else
	      parent = stk_.Top();
	   wasLeft = (parent != nullptr && parent->HasLeftChild() && loc == parent->lchild_);
        }while(wasLeft); // location was the right child of parent
    }

  }

  template < class C >
  InorderBTIterator<C>&  InorderBTIterator<C>::operator++ ()
  {
    // fsu::Debug("operator++()");
    do Increment();
    while (!stk_.Empty() && stk_.Top()->IsDead());
    return *this;
  }

  template < class C >
  InorderBTIterator<C>   InorderBTIterator<C>::operator++ (int)
  {
    InorderBTIterator<C> i(*this);
    this->operator++();
    return i;
  }

  template < class C >
  InorderBTIterator<C>&  InorderBTIterator<C>::operator-- ()
  {
    // fsu::Debug("operator--()");
    do Decrement();
    while (!stk_.Empty() && stk_.Top()->IsDead());
    return *this;
  }

  template < class C >
  InorderBTIterator<C>   InorderBTIterator<C>::operator-- (int)
  {
    InorderBTIterator<C> i(*this);
    this->operator--();
    return i;
  }

} // namespace fsu 

#endif

