#!/bin/sh
sm2m.x m200x300.sm m200x300.mat # convert sm to matrix
sv2v.x v1x500.sv v1x500.v # convert sv to vector
mxv.x m200x300.mat v1x500.v # multiplication
v2sv.x matxvec.v matxvec.sv 
clear
smxsv.x m200x300.sm v1x500.sv multi.sv
sv2v.x multi.sv multi.v
