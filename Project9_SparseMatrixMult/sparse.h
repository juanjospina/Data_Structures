/*

   @file    sparse.h
   @author  Juan Jose Ospina
   @date    2015-07-26
   @version 1.0

   Sparse file contains the declarations and implementations of Sparse Vector
   and Sparse Matrix (implemented as Vector of SparseVectors). These Sparse 
   Vector and Matrix are implemented using Hash Tables as base container for
   saving the data.

   Two Global operators * are declared and implemented for the Sparse Vectors 
   and Sparse Matrices. 

*/

#include <iostream>
#include <iomanip>
#include <fstream>

#include <hashtbl.h>
#include <hashclasses.h>
#include <hashfunctions.h>

#ifndef _SPARSE_H
#define _SPARSE_H

namespace fsu
{

  //---------------------
  //   class SparseVector
  //---------------------

  template < typename N >
  class SparseVector
  {

  public:
    typedef N  ValueType;
    ValueType&       operator [] (size_t i)       { return val_[i]; }
    const ValueType& operator [] (size_t i) const { return val_[i]; }

    typedef size_t                                             KeyType;
    typedef N                                                  DataType;
    typedef hashclass::KISS < size_t >                         HashType;
    typedef fsu::Entry      < KeyType , DataType >             EntryType;
    typedef fsu::List       < EntryType >                      BucketType;
    typedef fsu::HashTable  < KeyType , DataType , HashType >  TableType;
    typedef typename TableType::Iterator                       Iterator;

    Iterator Begin () const { return val_.Begin(); }
    Iterator End   () const { return val_.End(); }

    bool     Retrieve   (size_t i, N& n) const { return val_.Retrieve(i,n); }
    size_t   NumEntries () const { return val_.Size(); }
    size_t   MaxIndex   () const;

    explicit SparseVector      (size_t size = 100) : val_(size) {}
    SparseVector               (const SparseVector& v) : val_(v.val_) {}
    virtual ~SparseVector      () {}
    SparseVector<N>& operator= (const SparseVector<N>& v) { val_ = v.val_; return *this; }
    void Clear                 () { val_.Clear(); }

    void Rehash ( size_t size = 0 ) { val_.Rehash(size); }

  private: 
    TableType val_;
  };

  template < typename N >
  size_t SparseVector<N>::MaxIndex () const
  {
    size_t max = 0; 
    for (Iterator i = Begin(); i != End(); ++i)
      if ((*i).key_ > max) max = (*i).key_;
    return max;
  }

  //---------------------
  //   class SparseMatrix
  //---------------------

  template < typename N >
  class SparseMatrix
  {

  public:
    typedef N                   ValueType;
    typedef SparseVector<N>     RowType;

    // element access
    RowType&       operator   [] (size_t i)                 { return row_[i]; }
    const RowType& operator   [] (size_t i) const           { return row_[i]; }
    ValueType& operator       () (size_t i, size_t j)       { return (*this)[i][j]; }
    const ValueType& operator () (size_t i, size_t j) const { return (*this)[i][j]; }

    // proper type
    explicit SparseMatrix (size_t numrows = 100) : row_(numrows) {}
    SparseMatrix (const SparseMatrix& m) : row_(m.row_) {}
    virtual ~SparseMatrix() { Clear(); }
    void Clear() { row_.Clear(); }
    SparseMatrix<N>& operator=  (const SparseMatrix<N>& m) { row_ = m.row_; return *this; }

    // other terminology support
    typedef size_t                                             KeyType;
    typedef RowType                                            DataType;
    typedef hashclass::KISS < size_t >                         HashType;
    typedef fsu::Entry      < KeyType , DataType >             EntryType;
    typedef fsu::HashTable  < KeyType , DataType , HashType >  TableType;
    typedef typename TableType::Iterator Iterator;

    // iterator support
    Iterator Begin () const { return row_.Begin(); }
    Iterator End   () const { return row_.End(); }

    // informational
    size_t NumEntries() const;
    fsu::Pair<size_t, size_t> MaxIndices() const;
    bool Retrieve (size_t i, size_t j, N& n) const;

    // improve structural efficiency
    void Rehash ( size_t size = 0 );

  private: 
    fsu::HashTable < size_t , SparseVector<N> , hashclass::KISS < size_t > > row_;
  };

  template < typename N >
  bool SparseMatrix<N>::Retrieve (size_t i, size_t j, N& n) const
  {
    RowType r;
    if (!row_.Retrieve(i,r))
      return 0;
    if (!r.Retrieve(j,n))
      return 0;
    return 1;
  }

  template < typename N >
  size_t SparseMatrix<N>::NumEntries() const
  {
    size_t count(0);
    for (Iterator i = row_.Begin(); i != row_.End(); ++i)
    {
      count += (*i).data_.NumEntries();
    }
    return count;
  }

  template < typename N >
  fsu::Pair<size_t, size_t> SparseMatrix<N>::MaxIndices() const
  {
    fsu::Pair<size_t,size_t> p(0,0);
    for (Iterator i = row_.Begin(); i != row_.End(); ++i)
    {
      if (p.first_ < (*i).key_) p.first_ = (*i).key_;
      for (typename RowType::Iterator j = (*i).data_.Begin(); j != (*i).data_.End(); ++j)
        if (p.second_ < (*j).key_) p.second_ = (*j).key_;
    }
    return p;
  }
    
  template < typename N >
  void SparseMatrix<N>::Rehash ( size_t size )
  {
    for (Iterator i = row_.Begin(); i != row_.End(); ++i)
    {
      (*i).data_.Rehash();
    }
    row_.Rehash(size);
  }

} // namespace fsu


//---------------------
//   global operators
//---------------------


/*
*   Function : operator*(Sparse Matrix, Sparse Vector) 
*   Description: Performs the operator * referred as matrix
*   -vector multiplication.
*   Date: July 28, 2015
*/

template < typename N >
fsu::SparseVector<N> operator* (const fsu::SparseMatrix<N>& a, const fsu::SparseVector<N>& v)
{

  fsu::SparseVector <N> w;
  typename fsu::SparseMatrix<N>::Iterator row;
  typename fsu::SparseMatrix<N>::RowType::Iterator cl;
  N u;
  N t;

  // Checking Size to coincide
  //size_t sizeMult = a.NumEntries();
  /*if(sizeMult != v.NumEntries())
  {
     std::cerr << " ** SMxSV Multiplication Error: Rows of SM and SV are not the same \n";
     return w;
  }*/

  row = a.Begin();
  cl = (*row).data_.Begin();

  for(row = a.Begin(); row != a.End();  ++row)
  {    
     for(cl = v.Begin(); cl != v.End(); ++cl)
     {
        if (a.Retrieve((*row).key_,(*cl).key_,u)&& v.Retrieve((*cl).key_,t))
        {
           w[(*row).key_] += u*t; 
        }
     }

  }
  return w;
} // SM*SV */

template < typename N >
fsu::SparseMatrix<N> operator* (const fsu::SparseMatrix<N>& a, const fsu::SparseMatrix<N>& b)
{
  fsu::SparseMatrix <N> x;
  typename fsu::SparseMatrix<N>::Iterator row;
  typename fsu::SparseMatrix<N>::Iterator column;
  row = a.Begin();
  column = b.Begin();
  return x;
} // SM*SM */

#endif
