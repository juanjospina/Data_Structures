#!/bin/sh
sm2m.x m800x800.sm m800x800.mat # convert sm to matrix
sv2v.x v1x800.sv v1x800.v # convert sv to vector
mxv.x m800x800.mat v1x800.v # multiplication
v2sv.x matxvec.v matxvec.sv 
clear
smxsv.x m800x800.sm v1x800.sv multi.sv
sv2v.x multi.sv multi.v
