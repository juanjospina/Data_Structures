/*
*     @file    wordbench.cpp
*     @author  Juan Jose Ospina
*     @date    2015-06-14
*     @version 1.0
*/                 


#include <iomanip>
#include <wordify.cpp>
#include <wordbench.h>
#include <fstream>


/*
*    Function : ReadText()
*    Description: Reads the strings of a file
*    and enters them into a structure. wordbench
*    Author: Juan Jose Ospina
*    Date: June 14, 2015
*/


bool WordBench::ReadText(const fsu::String& infile)
{

   std::ifstream in1;
   fsu::String tempStr;
   int counter = 0;
	
   EntryType inserted;
	
   typename SetType::Iterator  i;
   typename SetType::Iterator  j;

   in1.open(infile.Cstr());
   if(!in1)
      return 0;

   infiles_.PushBack(infile);

   while(in1 >> tempStr)
   {
      if(fsu::String::StrCmp(tempStr, "") != 0)
      {
         Wordify(tempStr);     
	   
         if(fsu::String::StrCmp(tempStr,"\0") != 0) //Removes from vocaulary
         {
	      
            ++counter;
            inserted.first_ = tempStr;		 

            i = wordset_.Includes(inserted); 
            j = wordset_.End();

            if( i == j )
               inserted.second_ = 1;
            else
               inserted.second_ = 1 + (*i).second_;  

            wordset_.Insert(inserted);
         }
      }
   }


   //	Report Summary
   std::cout << "\n\n     Number of words read:    " << counter 
   << "\n     Current vocabulary size: " << wordset_.Size()
   << "\n\n";


   if(wordset_.Empty()== true)
      return 0;
   else 
      return 1;

}

/*
*    Function : WriteReport()
*    Description: Writes the report of the wordbench
*    into a file provided by the user.
*    Author: Juan Jose Ospina
*    Date: June 14, 2015
*/

bool WordBench::WriteReport(const fsu::String& outfile, unsigned short c1, unsigned short c2) const
{


   // Output variables
   const char ofr = ' ';
   int counter = 0;     

   std::ofstream out1(outfile.Cstr());
   if (out1.fail())
     {
        std::cout << " ** cannot open file " << outfile << " for write\n"
	<< " ** try again\n";
	return 0;
     }

   out1 << "Text Analysis for files: " ;
   infiles_.Display(out1, ofr);

   out1 << "\n\nword" << std::setw(c1)  << "frequency\n"
       << "----" << std::setw(c2) << "--------\n";

   typename SetType::Iterator  i;

   for(i = wordset_.Begin(); i != wordset_.End(); ++i)
   {
      out1 << std::setw(c1) << std::left << (*i).first_ 
      << std::setw(c2) << std::left  
      << (*i).second_ << "\n";

      counter += (*i).second_;

   }

   out1 <<"\n\nNumber of words: " << counter 
   << "\nVocabulary size: " << wordset_.Size() << std::endl;

   std::cout <<"\n     Number of words: " << counter
      << "\n     Vocabulary size: " << wordset_.Size() <<std::endl
      << "     Analyis written to file " << outfile << "\n\n"; 


   return 1;

}


/*
*    Function : ShowSummary()
*    Description: Shows summary of wordbench
*    into standard output - screen.
*    Author: Juan Jose Ospina
*    Date: June 14, 2015
*/

void WordBench::ShowSummary () const
{

   // Output variables
   std::ostream* osr = &std::cout;
   const char ofr = ',';
   
   int counter = 0;
   typename SetType::Iterator  i;
   
   for(i = wordset_.Begin(); i != wordset_.End(); ++i)
      counter += (*i).second_; 


   std::cout << "\n     Current Files: ";
   infiles_.Display(*osr, ofr);
   std::cout << "\n     Current word count:    " << counter 
   << "\n     Current vocabulary size: " << wordset_.Size() << "\n";



}

/*
*    Function : ClearData()
*    Description: Clear all data
*    Author: Juan Jose Ospina
*    Date: June 14, 2015
*/

void WordBench::ClearData()
{

   // Clear List
   infiles_.Clear();
   //Clear Set
   wordset_.Clear();

}




