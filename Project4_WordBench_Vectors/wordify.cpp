/*
*     @file   wordify.cpp 
*     @author  Juan Jose Ospina
*     @date    2015-06-13
*     @version 1.5
*
*/


#include <xstring.h>

/*
*    Function : Wordify()
*    Description: Cleans up a word
*    according to some rules given.
*    Author: Juan Jose Ospina
*    Date: June 17, 2015
*    Modified: June 28, 2015
*    cleanup words all working correctly.
*/


static void Wordify(fsu::String& str)
{

   int i = 0;
   int j = 0;
   int start = 0;
   int stop = 0;

   if (!(isalnum(str.Element(i))))
   {  
      if(str.Element(i+1) == '\0')
      {
         str[0] = '\0';
         str.SetSize(0);
      }
   }

   while(str.Element(i) != '\0')
   {
      
      if(isalnum(str.Element(i)) ||str.Element(i) == '\\' || ((str.Element(i) == '-') && isdigit(str.Element(i+1))))
      {  
         start = i;
         stop = i;
         j = i;

         while(str.Element(stop) != '\0')
         {
           char tempL = str.Element(stop+1);
           //std::cout <<"Aqui: " << str.Element(stop);

           if(tempL == '\0' )
              break;

           if(tempL == ',')
           {
              if(isdigit(str.Element(stop)) && isdigit(str.Element(stop+2)))
                 ++stop;
	      else 
                  break;
           }

	   else if(tempL == ':')
           {
              if(isdigit(str.Element(stop)) && isdigit(str.Element(stop+2)))
                 ++stop;
	      else if(isalnum(str.Element(stop+2)))
	         ++stop;
	      else if(str.Element(stop+2) == ':')
	      {
	         if(isalnum(str.Element(stop+3)))
	            ++stop;
		 else
		    break;
              }
	      else 
                  break;
           }

           else if(tempL == '\\' || tempL == '\'' || tempL == '-' || tempL == '.')
           {     
              if (isalnum(str.Element(stop+2)))
                 ++stop;
              else
                 break;
                           
           }else if(isalnum(tempL))
             ++stop;
           else
              break; 
        }

         while(j <= stop)
         {
            str[j-start] = tolower(str.Element(j));
            ++j;
         }

         break;
       }

      ++i;
   }
 
   if(stop == 0 && !(isalnum(str.Element(stop)))) 
   {
      str.SetSize(0);
   }else
   {
      int stop2 = stop+1; 
      str.SetSize(stop2-start);
   }
}
