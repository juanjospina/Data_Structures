Work Log
---------

June 03, 2015
-------------

Predicate class template LessThanSpy<T>

Data: 

- counter_ - a variable that stores the number of times the operator() is called.

functions:

- Constructor - which intializes counter to 0.

- operator(t1, t2) - returns true iff t1 < t2 - overloaded

- Reset - resets counter to 0 when called.

- Count() - Returns the counter_ variable.

Tested using sort_spy only(client).

-------------------------
gssearch.h
-------------------------

On namespace seq : store the 

g_lower_bound
g_upper_bound

June 04, 2015
-------------

Implementation of lower and upper bound.

// Lower Bound

template <class I, typename T, class P>
I g_lower_bound (I low, I high, const T& val, P& cmp)
{
	I temp;
	I temp2;

	temp = low;

	while(temp != high)
	{
	  	if(cmp(*temp, val))
			++temp;
			//temp++;
		else
		{
			temp2 = temp;
			break;
		}	
	}

	return temp2;

}


// Upper Bound

template <class I, typename T, class P>
I g_upper_bound (I low, I high, const T& val, P& cmp)
{

	I temp;
	I temp2;

	temp = low;

	while(temp != high)
	{
	  	if(cmp(*temp, val) || *temp == val)
			++temp;
			//temp++;
		else
		{
			temp2 = temp;
			break;
		}	
	}

	return temp2;

}


// After testing, this implementation works for most of the tests but failed
while testing with search_spy_uint32.com1 (without silent)
It was found that there was a malfunction with these implementations, the use
of temp2. Temp2 was crashing the program on certain occurences. 

To solve the issue, temp2 was removed and all the test worked correctly.
The new implementations of lower and upper bounds are presented next: 


template <class I, typename T, class P>
I g_lower_bound (I low, I high, const T& val, P& cmp)
{

        I temp;

        temp = low;

        while(temp != high)
        {
                if(cmp(*temp, val))
                        ++temp;
                else
                        break;
        }

        return temp;

}



template <class I, typename T, class P>
I g_upper_bound (I low, I high, const T& val, P& cmp)
{

        I temp;

        temp = low;

        while(temp != high)
        {

                if(cmp(*temp, val) || *temp == val)
                                ++temp;
                else
                        break;

        }

        return temp;

}

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-----------------------------------------------------------------------------
 Testing Scenarios

June 05, 2015

search_spy.x(personal)- Result 
vs. 
search_spy_uint32_i.x(given by Instructor) - Expected
----------------------------------------

1)Test: search_spy_uint32.com1

  Expected : 
  ----------


           algorithm   min comps   avg comps   max comps   size  log size[ceiling]  trials
           ---------   ---------   ---------   ---------   ----  --------           ------
  fsu::g_lower_bound           5        5.35           6     37         6               23
  fsu::g_upper_bound           5        5.30           6     37         6               23
  alt::g_lower_bound           5        5.35           6     37         6               23
  alt::g_upper_bound           5        5.30           6     37         6               23
  seq::g_lower_bound           1       23.70          37     37         6               23
  seq::g_upper_bound           3       25.26          37     37         6               23

  

  

  Result : 
  --------

           algorithm   min comps   avg comps   max comps   size  log size[ceiling]  trials
           ---------   ---------   ---------   ---------   ----  --------           ------
  fsu::g_lower_bound           5        5.35           6     37         6               23
  fsu::g_upper_bound           5        5.30           6     37         6               23
  alt::g_lower_bound           5        5.35           6     37         6               23
  alt::g_upper_bound           5        5.30           6     37         6               23
  seq::g_lower_bound           1       23.70          37     37         6               23
  seq::g_upper_bound           3       25.26          37     37         6               23


  Passed: YES
 ----------------

-------------------------------------------


2)Test: search_spy_uint32.com2

  Expected : 
  ----------

           algorithm   min comps   avg comps   max comps   size  log size[ceiling]  trials
           ---------   ---------   ---------   ---------   ----  --------           ------
  fsu::g_lower_bound          13       13.38          14  10000        14             1000
  fsu::g_upper_bound          13       13.39          14  10000        14             1000
  alt::g_lower_bound          13       13.38          14  10000        14             1000
  alt::g_upper_bound          13       13.39          14  10000        14             1000
  seq::g_lower_bound           1     5001.68        9989  10000        14             1000
  seq::g_upper_bound           1     5001.77        9989  10000        14             1000


  Result : 
  --------

           algorithm   min comps   avg comps   max comps   size  log size[ceiling]  trials
           ---------   ---------   ---------   ---------   ----  --------           ------
  fsu::g_lower_bound          13       13.38          14  10000        14             1000
  fsu::g_upper_bound          13       13.39          14  10000        14             1000
  alt::g_lower_bound          13       13.38          14  10000        14             1000
  alt::g_upper_bound          13       13.39          14  10000        14             1000
  seq::g_lower_bound           1     5001.68        9989  10000        14             1000
  seq::g_upper_bound           1     5001.77        9989  10000        14             1000



  Passed: YES
 ----------------



-------------------------------------------


3)Test: search_spy_uint32.com3

  Expected : 
  ----------
           algorithm   min comps   avg comps   max comps   size  log size[ceiling]  trials
           ---------   ---------   ---------   ---------   ----  --------           ------
  fsu::g_lower_bound          16       16.69          17 100000        17            10000
  fsu::g_upper_bound          16       16.69          17 100000        17            10000
  alt::g_lower_bound          16       16.69          17 100000        17            10000
  alt::g_upper_bound          16       16.69          17 100000        17            10000
  seq::g_lower_bound           3    49802.73       99998 100000        17            10000
  seq::g_upper_bound           3    49802.83       99998 100000        17            10000


  Result : 
  --------

           algorithm   min comps   avg comps   max comps   size  log size[ceiling]  trials
           ---------   ---------   ---------   ---------   ----  --------           ------
  fsu::g_lower_bound          16       16.69          17 100000        17            10000
  fsu::g_upper_bound          16       16.69          17 100000        17            10000
  alt::g_lower_bound          16       16.69          17 100000        17            10000
  alt::g_upper_bound          16       16.69          17 100000        17            10000
  seq::g_lower_bound           3    49802.73       99998 100000        17            10000
  seq::g_upper_bound           3    49802.83       99998 100000        17            10000



  Passed: YES
 ----------------



-------------------------------------------
-------------------------------------------
search_spy.x(personal)- Result 
vs. 
search_spy_char_i.x(given by Instructor) - Expected
----------------------------------------

4)Test: search_spy_char.com1

  Expected : 
  ----------

            algorithm   min comps   avg comps   max comps   size  log size[ceiling]  trials
           ---------   ---------   ---------   ---------   ----  --------           ------
  fsu::g_lower_bound           4        4.73           5     25         5               26
  fsu::g_upper_bound           4        4.69           5     25         5               26
  alt::g_lower_bound           4        4.73           5     25         5               26
  alt::g_upper_bound           4        4.69           5     25         5               26
  seq::g_lower_bound           1       15.46          25     25         5               26
  seq::g_upper_bound           1       16.38          25     25         5               26



  Result : 
  --------

           algorithm   min comps   avg comps   max comps   size  log size[ceiling]  trials
           ---------   ---------   ---------   ---------   ----  --------           ------
  fsu::g_lower_bound           4        4.73           5     25         5               26
  fsu::g_upper_bound           4        4.69           5     25         5               26
  alt::g_lower_bound           4        4.73           5     25         5               26
  alt::g_upper_bound           4        4.69           5     25         5               26
  seq::g_lower_bound           1       15.46          25     25         5               26
  seq::g_upper_bound           1       16.38          25     25         5               26


  Passed: YES
 ----------------

---------------------------------
5)Test: search_spy_char.com2

  Expected : 
  ----------
           algorithm   min comps   avg comps   max comps   size  log size[ceiling]  trials
           ---------   ---------   ---------   ---------   ----  --------           ------
  fsu::g_lower_bound           6        6.05           7     64         6               38
  fsu::g_upper_bound           6        6.03           7     64         6               38
  alt::g_lower_bound           6        6.05           7     64         6               38
  alt::g_upper_bound           6        6.03           7     64         6               38
  seq::g_lower_bound           1       28.05          63     64         6               38
  seq::g_upper_bound           2       29.71          64     64         6               38


  Result : 
  --------

           algorithm   min comps   avg comps   max comps   size  log size[ceiling]  trials
           ---------   ---------   ---------   ---------   ----  --------           ------
  fsu::g_lower_bound           6        6.05           7     64         6               38
  fsu::g_upper_bound           6        6.03           7     64         6               38
  alt::g_lower_bound           6        6.05           7     64         6               38
  alt::g_upper_bound           6        6.03           7     64         6               38
  seq::g_lower_bound           1       28.05          63     64         6               38
  seq::g_upper_bound           2       29.71          64     64         6               38


  Passed: YES
 ----------------

---------------------------------
6)Test: search_spy_char.com3

  Expected : 
  ----------
                algorithm   min comps   avg comps   max comps   size  log size[ceiling]  trials
           ---------   ---------   ---------   ---------   ----  --------           ------
  fsu::g_lower_bound           6        6.60           7     90         7              183
  fsu::g_upper_bound           6        6.68           7     90         7              183
  alt::g_lower_bound           6        6.60           7     90         7              183
  alt::g_upper_bound           6        6.68           7     90         7              183
  seq::g_lower_bound           1       40.37          90     90         7              183
  seq::g_upper_bound           1       42.22          90     90         7              183
  


  Result : 
  --------

             algorithm   min comps   avg comps   max comps   size  log size[ceiling]  trials
           ---------   ---------   ---------   ---------   ----  --------           ------
  fsu::g_lower_bound           6        6.60           7     90         7              183
  fsu::g_upper_bound           6        6.68           7     90         7              183
  alt::g_lower_bound           6        6.60           7     90         7              183
  alt::g_upper_bound           6        6.68           7     90         7              183
  seq::g_lower_bound           1       40.37          90     90         7              183
  seq::g_upper_bound           1       42.22          90     90         7              183


  Passed: YES
 ----------------

---------------------------------
7)Test: Entered juanospina

  Expected : 
  ----------
  
Enter sentinel: .
Enter elements ('.' to end): juanospina.
D as entered:                juanospina
L as entered:                juanospina
g_heap_sort(<):              aaijnnopsu
List::Sort(<):               aaijnnopsu
Enter search value ('.' to quit): j
 fsu::g_lower_bound on Deque
  D = aaijnnopsu
         ^lb
 fsu::g_upper_bound on Deque
  D = aaijnnopsu
          ^ub
 alt::g_lower_bound on Deque
  D = aaijnnopsu
         ^lb
 alt::g_upper_bound on Deque
  D = aaijnnopsu
          ^ub
 seq::g_lower_bound on List
  L = aaijnnopsu
         ^lb
 seq::g_upper_bound on List
  L = aaijnnopsu
          ^ub
Enter search value ('.' to quit): a
 fsu::g_lower_bound on Deque
  D = aaijnnopsu
      ^lb
 fsu::g_upper_bound on Deque
  D = aaijnnopsu
        ^ub
 alt::g_lower_bound on Deque
  D = aaijnnopsu
      ^lb
 alt::g_upper_bound on Deque
  D = aaijnnopsu
        ^ub
 seq::g_lower_bound on List
  L = aaijnnopsu
      ^lb
 seq::g_upper_bound on List
  L = aaijnnopsu
        ^ub
Enter search value ('.' to quit): n
 fsu::g_lower_bound on Deque
  D = aaijnnopsu
          ^lb
 fsu::g_upper_bound on Deque
  D = aaijnnopsu
            ^ub
 alt::g_lower_bound on Deque
  D = aaijnnopsu
          ^lb
 alt::g_upper_bound on Deque
  D = aaijnnopsu
            ^ub
 seq::g_lower_bound on List
  L = aaijnnopsu
          ^lb
 seq::g_upper_bound on List
  L = aaijnnopsu
            ^ub
Enter search value ('.' to quit): z
 fsu::g_lower_bound on Deque
  D = aaijnnopsu
                ^lb
 fsu::g_upper_bound on Deque
  D = aaijnnopsu
                ^ub
 alt::g_lower_bound on Deque
  D = aaijnnopsu
                ^lb
 alt::g_upper_bound on Deque
  D = aaijnnopsu
                ^ub
 seq::g_lower_bound on List
  L = aaijnnopsu
                ^lb
 seq::g_upper_bound on List
  L = aaijnnopsu
                ^ub
Enter search value ('.' to quit): .
  


  Result : 
  --------

Enter sentinel: .
Enter elements ('.' to end): juanospina.
D as entered:                juanospina
L as entered:                juanospina
g_heap_sort(<):              aaijnnopsu
List::Sort(<):               aaijnnopsu
Enter search value ('.' to quit): j
 fsu::g_lower_bound on Deque
  D = aaijnnopsu
         ^lb
 fsu::g_upper_bound on Deque
  D = aaijnnopsu
          ^ub
 alt::g_lower_bound on Deque
  D = aaijnnopsu
         ^lb
 alt::g_upper_bound on Deque
  D = aaijnnopsu
          ^ub
 seq::g_lower_bound on List
  L = aaijnnopsu
         ^lb
 seq::g_upper_bound on List
  L = aaijnnopsu
          ^ub
Enter search value ('.' to quit): a
 fsu::g_lower_bound on Deque
  D = aaijnnopsu
      ^lb
 fsu::g_upper_bound on Deque
  D = aaijnnopsu
        ^ub
 alt::g_lower_bound on Deque
  D = aaijnnopsu
      ^lb
 alt::g_upper_bound on Deque
  D = aaijnnopsu
        ^ub
 seq::g_lower_bound on List
  L = aaijnnopsu
      ^lb
 seq::g_upper_bound on List
  L = aaijnnopsu
        ^ub
Enter search value ('.' to quit): n
 fsu::g_lower_bound on Deque
  D = aaijnnopsu
          ^lb
 fsu::g_upper_bound on Deque
  D = aaijnnopsu
            ^ub
 alt::g_lower_bound on Deque
  D = aaijnnopsu
          ^lb
 alt::g_upper_bound on Deque
  D = aaijnnopsu
            ^ub
 seq::g_lower_bound on List
  L = aaijnnopsu
          ^lb
 seq::g_upper_bound on List
  L = aaijnnopsu
            ^ub
Enter search value ('.' to quit): z
 fsu::g_lower_bound on Deque
  D = aaijnnopsu
                ^lb
 fsu::g_upper_bound on Deque
  D = aaijnnopsu
                ^ub
 alt::g_lower_bound on Deque
  D = aaijnnopsu
                ^lb
 alt::g_upper_bound on Deque
  D = aaijnnopsu
                ^ub
 seq::g_lower_bound on List
  L = aaijnnopsu
                ^lb
 seq::g_upper_bound on List
  L = aaijnnopsu
                ^ub
Enter search value ('.' to quit): .


  Passed: YES
 ----------------

-------------------------------------------
SORTING SECTION TESTING
-------------------------------------------
sort_spy.x(personal)- Result 
vs. 
sort_spy_i.x(given by Instructor) - Expected
ALL n.* files were generated by ranunint.cpp
----------------------------------------

8)Test: n.100

  Expected : 
  ----------
-n.100----------------------------------------------

 Input file name: n.100
            size: 100

           algorithm     comp_count         n   n log n    n(n+1)/2
           ---------     ----------       ---   -------    --------
    g_selection_sort           5050       100       664        5050
    g_insertion_sort           2521       100       664        5050
         g_heap_sort           1084       100       664        5050
          List::Sort            557       100       664        5050

 Process complete




  Result : 
  --------
-n.100----------------------------------------------

 Input file name: n.100
            size: 100

           algorithm     comp_count         n   n log n    n(n+1)/2
           ---------     ----------       ---   -------    --------
    g_selection_sort           5050       100       664        5050
    g_insertion_sort           2521       100       664        5050
         g_heap_sort           1084       100       664        5050
          List::Sort            557       100       664        5050

 Process complete

       

  Passed: YES
 ----------------

----------------------------------------

9)Test: n.1000

  Expected : 
  ----------
-n.1000----------------------------------------------

 Input file name: n.1000
            size: 1000

           algorithm     comp_count         n   n log n    n(n+1)/2
           ---------     ----------       ---   -------    --------
    g_selection_sort         500500      1000      9965      500500
    g_insertion_sort         248438      1000      9965      500500
         g_heap_sort          17193      1000      9965      500500
          List::Sort           8701      1000      9965      500500

 Process complete


  Result : 
  --------
-n.1000----------------------------------------------

 Input file name: n.1000
            size: 1000

           algorithm     comp_count         n   n log n    n(n+1)/2
           ---------     ----------       ---   -------    --------
    g_selection_sort         500500      1000      9965      500500
    g_insertion_sort         248438      1000      9965      500500
         g_heap_sort          17193      1000      9965      500500
          List::Sort           8701      1000      9965      500500

 Process complete

       

  Passed: YES
 ----------------

----------------------------------------

10)Test: n.10000

  Expected : 
  ----------

 -n.10000----------------------------------------------

 Input file name: n.10000
            size: 10000

           algorithm     comp_count         n   n log n    n(n+1)/2
           ---------     ----------       ---   -------    --------
    g_selection_sort       50005000     10000    132877    50005000
    g_insertion_sort       25145617     10000    132877    50005000
         g_heap_sort         239302     10000    132877    50005000
          List::Sort         123688     10000    132877    50005000

 Process complete

  Result : 
  --------

-n.10000----------------------------------------------

 Input file name: n.10000
            size: 10000

           algorithm     comp_count         n   n log n    n(n+1)/2
           ---------     ----------       ---   -------    --------
    g_selection_sort       50005000     10000    132877    50005000
    g_insertion_sort       25145617     10000    132877    50005000
         g_heap_sort         239302     10000    132877    50005000
          List::Sort         123688     10000    132877    50005000

 Process complete


  Passed: YES
 ----------------

----------------------------------------

11)Test: n.100000

  Expected : 
  ----------
 -n.100000----------------------------------------------

 Input file name: n.100000
            size: 100000

           algorithm     comp_count         n   n log n    n(n+1)/2
           ---------     ----------       ---   -------    --------
    g_selection_sort     5000050000    100000   1660964  5000050000
    g_insertion_sort     2499419154    100000   1660964  5000050000
         g_heap_sort        3058760    100000   1660964  5000050000
          List::Sort        1566458    100000   1660964  5000050000

 Process complete

 

  Result : 
  --------

 -n.100000----------------------------------------------

 Input file name: n.100000
            size: 100000

           algorithm     comp_count         n   n log n    n(n+1)/2
           ---------     ----------       ---   -------    --------
    g_selection_sort     5000050000    100000   1660964  5000050000
    g_insertion_sort     2499419154    100000   1660964  5000050000
         g_heap_sort        3058760    100000   1660964  5000050000
          List::Sort        1566458    100000   1660964  5000050000

 Process complete




  Passed: YES
 ----------------

----------------------------------------


