/**
    @file    compare_spy.h
    @author  Juan Jose Ospina
    @date    2015-06-03
    @version 0.9


    Predicate Class template.
    LessThanSpy<T> objects keep a count of the number of times the operator()
    is called since it was created or last time Reset function was called.
*/

#ifndef _COMPARESPY_H
#define _COMPARESPY_H

namespace fsu
{

template <typename T>
class LessThanSpy;

template <typename T>
class LessThanSpy
{
	public:
		
		// Default Constructor
		LessThanSpy():counter_(0) 
		{}

		// Function operator overloading
		bool operator () (const T& t1, const T& t2) 
		{
			counter_++;
			
			if (t1 < t2) 
				return true;
			else
				return false;
		}

		// Reset Function
		void Reset()
		{
			counter_ = 0;
		}

		// Count Function 
		size_t Count () const
		{
			return counter_;
		}

	private:

		size_t counter_; 	// stores number of times operator()
					// is called.
};

} // namespace fsu

#endif
