/**
    @file    gssearch.h
    @author  Juan Jose Ospina
    @date    2015-06-04
    @version 0.9

    Implementation of sequential (linear) search of
    g_lower_bound and g_upper_bound as generic algortihms.
 
    Both implementations are inside th namespace seq.

    Precondition: The range in which the operations are applied must be
    		  sorted.

    1. g_lower_bound(beg,end,t,p) returns the lower bound of t in 
    the range [beg,end) (using the predicate p to determine order).

    2. g_upper_bound(beg,end,t,p) returns the upper bound of t in 
    the range [beg,end) (using the predicate p to determine order).


*/

#ifndef _GSSEARCH_H
#define _GSSEARCH_H

#include <compare_spy.h>



namespace seq
{


/*
   Function : g_lower_bound()
   Description: Return lower bound of val
   		using sequential search.
   Author: Juan Jose Ospina
   Date: June 4, 2015
*/

template <class I, typename T, class P>
I g_lower_bound (I low, I high, const T& val, P& cmp)
{
	
	I temp;
	I temp2;

	temp = low;

	while(temp != high)
	{
	  	if(cmp(*temp, val))
			++temp;
			//temp++;
		else
		{
			temp2 = temp;
			break;
		}	
	}

	return temp2;

}


/*
   Function : g_upper_bound()
   Description: Return upper bound of val
   		using sequential search.
   Author: Juan Jose Ospina
   Date: June 4, 2015
*/

template <class I, typename T, class P>
I g_upper_bound (I low, I high, const T& val, P& cmp)
{

	I temp;
	I temp2;

	temp = low;

	while(temp != high)
	{
	  	if(cmp(*temp, val) || *temp == val)
			++temp;
			//temp++;
		else
		{
			temp2 = temp;
			break;
		}	
	}

	return temp2;

}


template <class I, typename T>
I g_lower_bound (I low, I high, const T& val)
{
     fsu::LessThanSpy<T> cmp;
     return seq::g_lower_bound(low, high, val, cmp); 
}
 
template <class I, typename T>
I g_upper_bound (I low, I high, const T& val)
{
     fsu::LessThanSpy<T> cmp;
     return seq::g_upper_bound(low, high, val, cmp);
}


} // end seq namespace

#endif
