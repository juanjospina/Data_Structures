/* 

   @file    pq.h
   @author  Juan Jose Ospina
   @date    2015-08-02
   @version 1.0

   This file contains the declaration and implementations of 6 similar Priority
   Queues, which behave the same but are implemented using different containers.

   Various implementations for PriorityQueue < T , P >
   organized by namespace as follows:

   nmsp  stbl container element order central algorithm      push     pop    front  
   ----  ---- --------- ------------- -------------------    ----     ---    -----
   pq1   yes  List      unordered     fsu::g_max_element()   O(1)     O(n)     O(n)
   pq2   yes  MOList    sorted        MOList::Insert()       O(n)     O(1)     O(1)
   pq3   no   Deque     unordered     fsu::g_max_element()   AO(1)    O(n)     O(n)
   pq4   yes  Deque     unordered     fsu::g_max_element()   AO(1)    O(n)     O(n)
   pq5   yes  MOVector  sorted        MOVector::Insert()     O(n)     O(1)     O(1)
   pq6   no   Vector    heap          fsu::g_push/pop_heap() O(log n) O(log n) O(1)

  All of the pq namespaces are defined in this file.
 
*/

#include <genalg.h>    // fsu::g_max_element()
#include <gheap.h>     // fsu::g_push_heap()    ,  fsu::g_pop_heap()
#include <list.h>      // fsu::List<>     ,  fsu::List<>::Iterator
#include <vector.h>    // fsu::Vector<>   ,  fsu::Vector<>::Iterator
#include <deque.h>     // fsu::Deque<>    ,  fsu::Deque<>::Iterator
#include <olist.h>     // fsu::MOList<>   ,  fsu::MOList<>::Iterator
#include <ovector.h>   // fsu::MOVector<> ,  fsu::MOVector<>::Iterator


/*
   Namespace: 	pq1
   Description: Priority Queue based on fsu::List <T> Container.
   		It stores elements in unsorted ordered List.
		Uses fsu::g_max_element() for various methods.
   Date: 02 August, 2015

*/

namespace pq1
{

  template <typename T, class P >
  class PriorityQueue
  {
    typedef typename fsu::List < T >             ContainerType;
    typedef T                                    ValueType;
    typedef P                                    PredicateType;
  
    PredicateType  p_;
    ContainerType  c_;
  
  public:
    PriorityQueue() : p_(), c_()
    {}
  
    explicit PriorityQueue(P p) : p_(p), c_()
    {}
  
    void Push (const T& t)
    {
      c_.PushBack(t);
    }
  
    void Pop ()
    {
      typedef typename ContainerType::ConstIterator IteratorType;
      IteratorType i = fsu::g_max_element (c_.Begin(), c_.End(), p_);
      c_.Remove(i); 

    }
  
    const T& Front () const
    {
      typedef typename ContainerType::ConstIterator IteratorType;
      IteratorType i = fsu::g_max_element (c_.Begin(), c_.End(), p_);
      return *i;
    }
  
    void Clear ()
    {
      c_.Clear();
    }
  
    bool Empty () const
    {
      return c_.Empty();
    }
  
    size_t Size () const
    {
      return c_.Size();
    }
  
    const P& GetPredicate() const
    {
      return p_;
    }
  
    void Dump (std::ostream& os, char ofc = '\0') const
    {
      c_.Display(os,ofc);
    }
  };
  
} // namespace pq1



/*
   Namespace: 	pq2
   Description: Priority Queue based on fsu::MOList < T , P > Container.
   		It stores elements in increasing  order in List.
   Date: 02 August, 2015

*/

namespace pq2
{

  template <typename T, class P >
  class PriorityQueue
  {
    typedef typename fsu::MOList < T , P >       ContainerType;
    typedef T                                    ValueType;
    typedef P                                    PredicateType;
  
    PredicateType  p_;
    ContainerType  c_;
  
  public:
    PriorityQueue() : p_(), c_()
    {}
  
    explicit PriorityQueue(P p) : p_(p), c_()
    {}
  
    void Push (const T& t)
    {
      c_.Insert(t);
    }
  
    void Pop ()
    {
      c_.PopBack();
    }
  
    const T& Front () const
    {
       return c_.Back();
    }
  
    void Clear ()
    {
      c_.Clear();
    }
  
    bool Empty () const
    {
      return c_.Empty();
    }
  
    size_t Size () const
    {
      return c_.Size();
    }
  
    const P& GetPredicate() const
    {
      return p_;
    }
  
    void Dump (std::ostream& os, char ofc = '\0') const
    {
      c_.Display(os,ofc);
    }
  };
  
} // namespace pq2


/*
   Namespace: 	pq3
   Description: Priority Queue based on fsu::Deque < T > Container.
   		It stores elements in a Deque. It differentiates from 
		pq4 in the way it Pops out elements. It copies the last
		element to popped element, and then uses PopBack()
   Date: 02 August, 2015

*/

namespace pq3
{

  template <typename T, class P >
  class PriorityQueue
  {
    typedef typename fsu::Deque < T >            ContainerType;
    typedef T                                    ValueType;
    typedef P                                    PredicateType;
  
    PredicateType  p_;
    ContainerType  c_;
  
  public:
    PriorityQueue() : p_(), c_()
    {}
  
    explicit PriorityQueue(P p) : p_(p), c_()
    {}
  
    void Push (const T& t)
    {
      c_.PushBack(t);
    }
  
    void Pop ()
    {
      typedef typename ContainerType::Iterator IteratorType;
      IteratorType i = fsu::g_max_element (c_.Begin(), c_.End(), p_);
      *i = c_.Back();   
      c_.PopBack();
    }
  
    const T& Front () const
    {
      typedef typename ContainerType::ConstIterator IteratorType;
      IteratorType i = fsu::g_max_element (c_.Begin(), c_.End(), p_);
      return *i;
    }
  
    void Clear ()
    {
      c_.Clear();
    }
  
    bool Empty () const
    {
      return c_.Empty();
    }
  
    size_t Size () const
    {
      return c_.Size();
    }
  
    const P& GetPredicate() const
    {
      return p_;
    }
  
    void Dump (std::ostream& os, char ofc = '\0') const
    {
      c_.Display(os,ofc);
    }
  };
  
} // namespace pq3

/*
   Namespace: 	pq4
   Description: Priority Queue based on fsu::Deque < T > Container.
   		It stores elements in a Deque. It differentiates from 
		pq3 in the way it Pops out elements. It performs a "leapfrog"
		copy down of elements, sarting at the popped element. It uses
		fsu::g_max_element().
   Date: 02 August, 2015

*/
namespace pq4
{

  template <typename T, class P >
  class PriorityQueue
  {
    typedef typename fsu::Deque < T >            ContainerType;
    typedef T                                    ValueType;
    typedef P                                    PredicateType;
  
    PredicateType  p_;
    ContainerType  c_;
  
  public:
    PriorityQueue() : p_(), c_()
    {}
  
    explicit PriorityQueue(P p) : p_(p), c_()
    {}
  
    void Push (const T& t)
    {
      c_.PushBack(t);
    }
  
    void Pop ()
    {
      typedef typename ContainerType::Iterator IteratorType;
      IteratorType j = fsu::g_max_element (c_.Begin(), c_.End(), p_);
      size_t m = j - c_.Begin();
      for (size_t i = m; i <  c_.Size() - 1 ; ++i)
        c_[i] = c_[i+1];
      c_.PopBack();
    }
  
    const T& Front () const
    {
      typedef typename ContainerType::ConstIterator IteratorType;
      IteratorType i = fsu::g_max_element (c_.Begin(), c_.End(), p_);
      return *i;
    }
  
    void Clear ()
    {
      c_.Clear();
    }
  
    bool Empty () const
    {
      return c_.Empty();
    }
  
    size_t Size () const
    {
      return c_.Size();
    }
  
    const P& GetPredicate() const
    {
      return p_;
    }
  
    void Dump (std::ostream& os, char ofc = '\0') const
    {
      c_.Display(os,ofc);
    }
  };
  
} // namespace pq4


/*
   Namespace: 	pq5
   Description: Priority Queue based on fsu::MOVector < T, P > Container.
   		It stores elements in an increasing order Vector.
   Date: 02 August, 2015

*/

namespace pq5
{

  template <typename T, class P >
  class PriorityQueue
  {
    typedef typename fsu::MOVector < T , P >     ContainerType;
    typedef T                                    ValueType;
    typedef P                                    PredicateType;
  
    PredicateType  p_;
    ContainerType  c_;
  
  public:
    PriorityQueue() : p_(), c_()
    {}
  
    explicit PriorityQueue(P p) : p_(p), c_()
    {}
  
    void Push (const T& t)
    {
      c_.Insert(t);
    }
  
    void Pop ()
    {
      c_.PopBack();
    }
  
    const T& Front () const
    {
       return c_.Back();
    }
  
    void Clear ()
    {
      c_.Clear();
    }
  
    bool Empty () const
    {
      return c_.Empty();
    }
  
    size_t Size () const
    {
      return c_.Size();
    }
  
    const P& GetPredicate() const
    {
      return p_;
    }
  
    void Dump (std::ostream& os, char ofc = '\0') const
    {
      c_.Display(os,ofc);
    }
  };
  
} // namespace pq5


/*
   Namespace: 	pq6
   Description: Priority Queue based on fsu::Vector < T > Container.
   		It stores elements in a Partial Ordered using Heap Algorithms.
		It uses fsu::g_push_heap(), fsu::g_pop_heap().
   Date: 02 August, 2015

*/

namespace pq6
{

  template <typename T, class P >
  class PriorityQueue
  {
    typedef typename fsu::Vector < T >           ContainerType;
    typedef T                                    ValueType;
    typedef P                                    PredicateType;
  
    PredicateType  p_;
    ContainerType  c_;
  
  public:
    PriorityQueue() : p_(), c_()
    {}
  
    explicit PriorityQueue(P p) : p_(p), c_()
    {}
  
    void Push (const T& t)
    {
      c_.PushBack(t);
      fsu::g_push_heap(c_.Begin(), c_.End(), p_);
    }
  
    void Pop ()
    {
      fsu::g_pop_heap(c_.Begin(), c_.End(), p_);
      c_.PopBack();

    }
  
    const T& Front () const
    {
       return c_.Front();
    }
  
    void Clear ()
    {
      c_.Clear();
    }
  
    bool Empty () const
    {
      return c_.Empty();
    }
  
    size_t Size () const
    {
      return c_.Size();
    }
  
    const P& GetPredicate() const
    {
      return p_;
    }
  
    void Dump (std::ostream& os, char ofc = '\0') const
    {
      c_.Display(os,ofc);
    }
  };
  
} // namespace pq6




