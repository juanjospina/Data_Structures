Development
02 August, 2015
----------------

nmsp  stbl container     push     pop    front        
   ----  ---- --------- -----     ---    -----      
   pq1   yes  List       O(1)     O(n)     O(n)     
   pq2   yes  MOList     O(n)     O(1)     O(1)     
   pq3   no   Deque      AO(1)    O(n)     O(n)     
   pq4   yes  Deque      AO(1)    O(n)     O(n)     
   pq5   yes  MOVector   O(n)     O(1)     O(1)     
   pq6   no   Vector     O(log n) O(log n) O(1)     

Informal Proofs:
------------------

push:
-----

pq1: O(1) because the PushBack() method found on List only performs an Insert 
without doing any traversal.

pq2: O(n) because the Insert() method used on the Push() performs a traversal 
checking or the ordered way of the List.

pq3: AO(1) because the methods makes use of the PushBack() ability of the Deque.


pq4: AO(1) because the methods makes use of the PushBack() ability of the Deque.

pq5: O(n) because it calls the method inserts from MOVector whhich perroms a traveral in order to Insert the new value.

pq6: O(log n) because after performing the PushBack() method form Vector, it 
performs a g_push_heap() in order to maintain the structure of the organization.

--------------------------------------------------------------------------------
pop:
-----

pq1: O(n) because the method uses the generic algorithm fsu::g_max_element() 
which its worst case is to traverse all the list.

pq2: O(1) because the method only pops back the last element in the vector, in which in this case will always be at the same position beacuse of the ordered quality.

pq3: O(n) because the method makes use of g_max_element() to perform the 
traversal in order to find th largest element in the Deque. It differentiates from pq4 in the way it Pops out elements. It copies the last element to popped element, and then uses PopBack()


pq4: O(n) because the method makes use of g_max_element() to perform the 
traversal in order to find th largest element in the Deque. It performs a "leapfrog" copy down of elements, sarting at the popped element.

pq5: O(1) because it uses PopBack() which doesnt need to perform a traversal in
order to find the last element (max). Its ability to mantain partially ordered helps with this Pop().

pq6: O(log n) because before performing the PopBack() method from Vector, it 
performs a g_pop_heap() in order to maintain the structure of the organization.

--------------------------------------------------------------------------------
front:
-----

pq1: O(n) because the method uses the generic algorithm fsu::g_max_element() 
which its worst case is to traverse all the list.

pq2: O(1) because the method only returns the Back() element in the vector, in which in this case will always be at the same position beacuse of the ordered quality.

pq3: O(n) because the method makes use of g_max_element() to perform the 
traversal in order to find th largest element in the Deque. It differentiates from pq4 in the way it Pops out elements. It copies the last element to popped element, and then uses PopBack()


pq4: O(n) because the method makes use of g_max_element() to perform the 
traversal in order to find th largest element in the Deque.  It differentiates from pq3 in the way it Pops out elements. It performs a "leapfrog" copy down of elements, sarting at the popped element.

pq5:O(1) because it returns Back() which doesnt need to perform a traversal in
order to find the last element (max). Its ability to mantain partially ordered helps with this Pop().

pq6: O(1) because it returns Front() which doesnt need to perform a traversal in
order to find the last element (max). Its ability to mantain partially ordered structure it is known that the Front element will be always the one ith high 
priority, thanks to the generic heap algorithms.

----------------------------------------------------------------------------------
---------------------------------------------------------------------------------
TESTING
--------
TestBench
-----------
Procedures
------------
1) Run check.fpq fpq.com1
2) Run check.pqsort pqsort.data1

1)-------------------------------------------

ospina@linprog2:~/cop4530/proj10/testbench> check.fpq fpq.com1 
ensuring testbed directory ...
copying files from parent directory ...
building pqtests (see "fpq.build.out" for build results) ...
copying area51 versions ...
testing with pqtests ...
creating diffs ...
test1 diff:
test2 diff:
test3 diff:
test4 diff:
test5 diff:
test6 diff:
see complete results in ftest?.out
end of check.fpq

----------------------------------------------------------
2)-------------------------------------------------------

ospina@linprog2:~/cop4530/proj10/testbench> check.pqsort pqsort.data1 
ensuring testbed directory ...
copying files from parent directory ...
building pqsorttests (see "pqsort.build.out" for build results) ...
 copying area51 versions ...
testing with pqsorttests ...
 creating diffs ...
sorttest1 diff:
sorttest2 diff:
sorttest3 diff:
sorttest4 diff:
sorttest5 diff:
sorttest6 diff:
see complete results in sorttest?.out
end of check.sorttest

-----------------------------------------------------------------------------
ADD: 
+ juan 12
+ paula 1
+ luke 2
+ lina 13
+ cami 56
+ robert 78
+ tino 1000
+ messi 5

: d
Q contents:
12:juan
1:paula
2:luke
13:lina
56:cami
78:robert
1000:tino
5:messi

Front:
Enter [command] [string] [priority] ('M' for menu, 'Q' to quit)
: f
Front of Q == tino

Enter [command] [string] [priority] ('M' for menu, 'Q' to quit)
: -
Popped 1000:tino from Q
Enter [command] [string] [priority] ('M' for menu, 'Q' to quit)
: f
Front of Q == robert
Enter [command] [string] [priority] ('M' for menu, 'Q' to quit)
: -
Popped 78:robert from Q
Enter [command] [string] [priority] ('M' for menu, 'Q' to quit)
: d
Q contents:
12:juan
1:paula
2:luke
13:lina
56:cami
5:messi
Enter [command] [string] [priority] ('M' for menu, 'Q' to quit)
: -
Popped 56:cami from Q
Enter [command] [string] [priority] ('M' for menu, 'Q' to quit)
: d
Q contents:
12:juan
1:paula
2:luke
13:lina
5:messi
Enter [command] [string] [priority] ('M' for menu, 'Q' to quit)
: -
Popped 13:lina from Q
Enter [command] [string] [priority] ('M' for menu, 'Q' to quit)
: f
Front of Q == juan
Enter [command] [string] [priority] ('M' for menu, 'Q' to quit)
: d
Q contents:
12:juan
1:paula
2:luke
5:messi
Enter [command] [string] [priority] ('M' for menu, 'Q' to quit)
: -
Popped 12:juan from Q
Enter [command] [string] [priority] ('M' for menu, 'Q' to quit)
: d
Q contents:
1:paula
2:luke
5:messi


Passed:
--------
pq1: Yes
pq2: Yes
pq3: Yes
pq4: Yes
pq5: Yes
pq6: Yes
